<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');
Route::get('/post/{id}', 'HomeController@post')->name('post');
Route::get('/page/{id}', 'HomeController@page')->name('page');
Route::get('/categories', 'HomeController@categories')->name('categories');
Route::get('/category/{id}', 'HomeController@category')->name('category');

Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {
	Route::view('/', 'admin.home')->name('home');
	Route::resource('/posts', 'Admin\PostController');
	Route::resource('/categories', 'Admin\CategoryController');
	Route::resource('/pages', 'Admin\PageController');
});