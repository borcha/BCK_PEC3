@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Listado de post en la categoria {{$category->name}}</div>
              <div class="card-body">
                <ul>
                @foreach($posts as $post)
                <li><a href="{{ route('post', $post->id) }}">{{$post->title}}</a></li>
                @endforeach

                {{$posts->links()}}
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
