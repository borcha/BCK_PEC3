@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Listado de categorías</div>
              <div class="card-body">
                <ul>
                @foreach($categories as $category)
                <li><a href="{{ route('category', $category->id) }}">{{$category->name}}</a></li>
                @endforeach
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
