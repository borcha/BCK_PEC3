@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-body">
                @foreach($posts as $post)
                <div class="post-preview">
                    <a href="{{route('post', $post->id)}}">
                        <h2 class="post-title">{{$post->title}}</h2>
                    </a>
                    <p class="post-meta">Publicado por {{$post->author->name}} el {{$post->created_at->format('d/m/Y')}}</p>
                </div>
                @endforeach

                {{ $posts->links() }}
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
