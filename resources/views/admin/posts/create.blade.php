@extends('layouts.admin')


@section('admin-title', 'Nuevo post')
@section('admin-content')

<form id="store-form" name="store-form" action="{{ route('admin.posts.store') }}" method="post">
  <div class="form-group">
    @csrf
    <label for="title">Título</label>
    <input type="text" name="title" class="form-control" id="title" placeholder="Título del post" required>
  </div>
  <div class="form-group">
    <label for="content">Contenido</label>
    <textarea class="form-control" id="content" name="content" rows="5" placeholder="Contenido del post" required></textarea>
  </div>
  <div class="form-group">
    <label for="categories">Categorías</label>
    <select multiple class="form-control" id="categories" name="categories[]">
      @foreach($categories as $category)
      <option value="{{$category->id}}">{{ $category->name }}</option>
      @endforeach
    </select>
  </div>
</form>
<button form="store-form" type="submit" class="btn btn-primary">Guardar</button> 
<a class="btn btn-secondary" href="{{ route('admin.posts.index') }}">Cancelar</a>

@endsection
