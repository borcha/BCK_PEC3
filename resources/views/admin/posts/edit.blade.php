@extends('layouts.admin')


@section('admin-title')
Editando post <strong>#{{$post->id}} {{$post->title}}</strong>
@endsection

@section('admin-content')

<form id="edit-form" name="edit-form" action="{{ route('admin.posts.update', $post->id) }}" method="post">
  <div class="form-group">
    @csrf
    @method('PUT')
    <label for="title">Título</label>
    <input type="text" name="title" class="form-control" id="title" placeholder="Título del post" value="{{$post->title}}" required>
  </div>
  <div class="form-group">
    <label for="content">Contenido</label>
    <textarea class="form-control" id="content" name="content" rows="5" required>{{$post->content}}</textarea>
  </div>
  <div class="form-group">
    <label for="categories">Categorías</label>
    <select multiple class="form-control" id="categories" name="categories[]">
      @foreach($categories as $category)
      <option value="{{$category->id}}" {{ ($post->categories->contains($category)) ? 'selected' : ''}}>{{ $category->name }}</option>
      @endforeach
    </select>
  </div>
</form>
<button form="edit-form" type="submit" class="btn btn-primary">Guardar</button> 
<a class="btn btn-secondary" href="{{ route('admin.posts.index') }}">Cancelar</a>
<button form="remove-form" class="btn btn-outline-danger float-right" type="submit" onclick="return confirm('¿Desea eliminar este post?')"><i class="fa fa-remove"></i> Eliminar</button>

<form id="remove-form" name="remove-form" action="{{ route('admin.posts.destroy', $post->id)}}" method="post">
@csrf
@method('DELETE')
</form>

@endsection
