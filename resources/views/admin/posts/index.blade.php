@extends('layouts.admin')


@section('admin-title', 'Listado de Posts')
@section('admin-content')

<p>Desde aquí podrá consultar la lista de Posts existentes además de crear, modificar y eliminar este tipo de contenido.</p>

<table class="table table-hover">
<thead class="thead-light">
<tr>
<th scope="col">#</th>
<th scope="col">Título</th>
<th scope="col">Autor</th>
<th scope="col" class="text-center">Fecha</th>
<th scope="col" class="text-center">Acción</th>
</tr>
</thead>
<tbody>
@foreach($posts as $post)
<tr>
<th scope="row">{{$post->id}}</th>
<td>{{$post->title}}</td>
<td>{{$post->author->name}}</td>
<td class="text-center"><time datetime="{{$post->updated_at}}" title="{{$post->updated_at}}">{{$post->updated_at->format('d/m/Y')}}</time></td>
<td class="text-center">
    <a href="{{ route('admin.posts.edit', $post->id) }}" title="Editar"><i class="fa fa-edit"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
{{ $posts->links() }}

<a href="{{ route('admin.posts.create')}}" class="btn btn-primary">Nuevo</a>
@endsection
