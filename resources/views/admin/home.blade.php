@extends('layouts.admin')


@section('admin-title', 'Inicio')
@section('admin-content')

    <p>Bienvenido al módulo de administración.</p>
    <p>Desde aquí podra administrar las distintas opciones y gestionar el contenido que aparece en el sitio. 
    Puede seleccionar las distintas opciones disponibles en el menú de la izquierda.</p>

@endsection
