@extends('layouts.admin')


@section('admin-title')
Editando página <strong>#{{$page->id}} {{$page->title}}</strong>
@endsection

@section('admin-content')

<form id="edit-form" name="edit-form" action="{{ route('admin.pages.update', $page->id) }}" method="post">
  <div class="form-group">
    @csrf
    @method('PUT')
    <label for="title">Título</label>
    <input type="text" name="title" class="form-control" id="title" placeholder="Título de la página" value="{{$page->title}}" required>
  </div>
  <div class="form-group">
    <label for="content">Contenido</label>
    <textarea class="form-control" id="content" name="content" rows="5" required>{{$page->content}}</textarea>
  </div>
</form>
<button form="edit-form" type="submit" class="btn btn-primary">Guardar</button> 
<a class="btn btn-secondary" href="{{ route('admin.pages.index') }}">Cancelar</a>
<button form="remove-form" class="btn btn-outline-danger float-right" type="submit" onclick="return confirm('¿Desea eliminar esta página?')"><i class="fa fa-remove"></i> Eliminar</button>

<form id="remove-form" name="remove-form" action="{{ route('admin.pages.destroy', $page->id)}}" method="post">
@csrf
@method('DELETE')
</form>

@endsection
