@extends('layouts.admin')


@section('admin-title', 'Nuevo página')
@section('admin-content')

<form id="store-form" name="store-form" action="{{ route('admin.pages.store') }}" method="post">
  <div class="form-group">
    @csrf
    <label for="title">Título</label>
    <input type="text" name="title" class="form-control" id="title" placeholder="Título de la página" required>
  </div>
  <div class="form-group">
    <label for="content">Contenido</label>
    <textarea class="form-control" id="content" name="content" rows="5" placeholder="Contenido de la página" required></textarea>
  </div>
</form>
<button form="store-form" type="submit" class="btn btn-primary">Guardar</button> 
<a class="btn btn-secondary" href="{{ route('admin.pages.index') }}">Cancelar</a>

@endsection
