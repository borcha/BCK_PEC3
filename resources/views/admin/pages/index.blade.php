@extends('layouts.admin')


@section('admin-title', 'Listado de páginas')
@section('admin-content')

<p>Desde aquí podrá consultar la lista de páginas existentes además de crear, modificar y eliminar este tipo de contenido.</p>

<table class="table table-hover">
<thead class="thead-light">
<tr>
<th scope="col">#</th>
<th scope="col">Título</th>
<th scope="col">Autor</th>
<th scope="col" class="text-center">Fecha</th>
<th scope="col" class="text-center">Acción</th>
</tr>
</thead>
<tbody>
@foreach($pages as $page)
<tr>
<th scope="row">{{$page->id}}</th>
<td>{{$page->title}}</td>
<td>{{$page->author->name}}</td>
<td class="text-center"><time datetime="{{$page->updated_at}}" title="{{$page->updated_at}}">{{$page->updated_at->format('d/m/Y')}}</time></td>
<td class="text-center">
    <a href="{{ route('admin.pages.edit', $page->id) }}" title="Editar"><i class="fa fa-edit"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
{{ $pages->links() }}

<a href="{{ route('admin.pages.create')}}" class="btn btn-primary">Nuevo</a>
@endsection
