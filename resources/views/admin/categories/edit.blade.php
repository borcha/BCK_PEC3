@extends('layouts.admin')


@section('admin-title')
Editando categoría <strong>#{{$category->name}}</strong>
@endsection

@section('admin-content')

<form id="edit-form" name="edit-form" action="{{ route('admin.categories.update', $category->id) }}" method="post">
  <div class="form-group">
    @csrf
    @method('PUT')
    <label for="name">Nombre</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre de la categoría" value="{{$category->name}}" required>
  </div>
</form>
<button form="edit-form" type="submit" class="btn btn-primary">Guardar</button> 
<a class="btn btn-secondary" href="{{ route('admin.categories.index') }}">Cancelar</a>
<button form="remove-form" class="btn btn-outline-danger float-right" type="submit" onclick="return confirm('¿Desea eliminar esta categoría?')"><i class="fa fa-remove"></i> Eliminar</button>

<form id="remove-form" name="remove-form" action="{{ route('admin.categories.destroy', $category->id)}}" method="post">
@csrf
@method('DELETE')
</form>

@endsection
