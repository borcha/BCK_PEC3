@extends('layouts.admin')


@section('admin-title', 'Listado de categorías')
@section('admin-content')

<p>Desde aquí podrá consultar la lista de categorías existentes.</p>

<table class="table table-hover">
<thead class="thead-light">
<tr>
<th scope="col">#</th>
<th scope="col">Nombre</th>
<th scope="col" class="text-center">Acción</th>
</tr>
</thead>
<tbody>
@foreach($categories as $category)
<tr>
<th scope="row">{{$category->id}}</th>
<td>{{$category->name}}</td>
<td class="text-center">
    <a href="{{ route('admin.categories.edit', $category->id) }}" title="Editar"><i class="fa fa-edit"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>

<a href="{{ route('admin.categories.create')}}" class="btn btn-primary">Nuevo</a>
@endsection
