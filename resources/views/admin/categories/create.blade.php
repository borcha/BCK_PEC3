@extends('layouts.admin')


@section('admin-title', 'Nueva categoría')
@section('admin-content')

<form id="store-form" name="store-form" action="{{ route('admin.categories.store') }}" method="post">
  <div class="form-group">
    @csrf
    <label for="name">Nombre</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre de la categoría" required>
  </div>
</form>
<button form="store-form" type="submit" class="btn btn-primary">Guardar</button> 
<a class="btn btn-secondary" href="{{ route('admin.categories.index') }}">Cancelar</a>

@endsection
