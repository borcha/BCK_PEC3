@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-body">
                <h1>{{$post->title}}</h1>
                <p class="text-muted">Publicado por {{$post->author->name}} el {{$post->created_at->format('d/m/Y')}}</p>
                @foreach($post->categories as $category)
                <a href="{{route('category', $category->id)}}" class="badge badge-primary">{{$category->name}}</a>
                @endforeach

<br>
<hr class="pb-4">

                <p>Delectus quae inventore quia in dolor. Asperiores accusantium illum non fuga accusamus ducimus. Esse provident laborum aut non.</p>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
