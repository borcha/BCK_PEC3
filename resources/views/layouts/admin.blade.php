@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">{{ __('Menú de administración') }}</div>

                <div class="card-body">
                    <div class="list-group list-group-flush">
                        @component('layouts.active-link', ['name' => 'admin.home']) Inicio @endcomponent
                        @component('layouts.active-link', ['name' => 'admin.posts.index', 'prefix'=>'admin.posts']) Posts @endcomponent
                        @component('layouts.active-link', ['name' => 'admin.categories.index', 'prefix'=>'admin.categories']) Categorías @endcomponent
                        @component('layouts.active-link', ['name' => 'admin.pages.index', 'prefix'=>'admin.pages']) Páginas @endcomponent
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">@yield('admin-title')</div>
                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            {{ $message }}
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Something went wrong<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                   @endif
                   
                	@yield('admin-content')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
