<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    protected $fillable = ['title','content','category_id','author_id'];

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'post_category');
    }
}
