<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;
use App\Page;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->simplePaginate(10);

        return view('home', compact('posts'));
            //->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function post(Request $request) {
        $post = Post::find($request->route('id'));
        return view('post', compact('post'));
    }

    public function page(Request $request) {
        $page = Page::find($request->route('id'));
        return view('page', compact('page'));
    }

    public function categories() {
        $categories = Category::all();
        return view('categories', compact('categories'));
    }

    public function category(Request $request) {
        $category = Category::find($request->route('id'));
        $posts = $category->posts()->simplePaginate(10);
        return view('category', compact('category', 'posts'));
    }

}
