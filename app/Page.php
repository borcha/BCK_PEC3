<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $fillable = ['title','content','author_id'];

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

}
